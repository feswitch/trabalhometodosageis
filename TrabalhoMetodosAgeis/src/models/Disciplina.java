package models;

import java.util.List;

public class Disciplina {

	private String nomeDisciplina;
	private String codDisciplina;
	private List<Inscrito> listaInscrito;

	public List<Inscrito> getListaInscrito() {
		return listaInscrito;
	}

	public void setListaInscrito(List<Inscrito> listaInscrito) {
		this.listaInscrito = listaInscrito;
	}

	public Disciplina(String nomeDisciplina, String codDisciplina) {
		this.nomeDisciplina = nomeDisciplina;
		this.codDisciplina = codDisciplina;
	}

	public Disciplina() {
	}

	public String getNomeDisciplina() {
		return nomeDisciplina;
	}

	public void setNomeDisciplina(String nomeDisciplina) {
		this.nomeDisciplina = nomeDisciplina;
	}

	public String getCodDisciplina() {
		return codDisciplina;
	}

	public void setCodDisciplina(String codDisciplina) {
		this.codDisciplina = codDisciplina;
	}
}
