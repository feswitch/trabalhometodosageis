package models;

public class Inscrito {

	private String cpf;
	private String nome;
	private String email;
	private Disciplina disciplina;

	public Inscrito(String cpf, String nome, String email, Disciplina disciplina) {
		super();
		this.cpf = cpf;
		this.nome = nome;
		this.email = email;
		this.disciplina = disciplina;
	}

	public Inscrito() {
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Disciplina getDisciplina() {
		return disciplina;
	}

	public void setDisciplina(Disciplina disciplina) {
		this.disciplina = disciplina;
	}

}